using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.name == "Character")
        {
            var health  = other.GetComponent<PlayerHealth>();
            health.Health = health.Health - 10; 

            iTween.ShakePosition(other.gameObject, Vector3.up, 0.3f);
        }
    }
             
}
