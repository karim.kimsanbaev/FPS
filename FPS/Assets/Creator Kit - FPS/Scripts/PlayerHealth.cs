using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public float  Health = 100;

    void Update()
    {
        if(Health <= 0)
        {
            GameSystem.Instance.StopTimer();
            GameSystem.Instance.FinishRun();
        }
    }
}
