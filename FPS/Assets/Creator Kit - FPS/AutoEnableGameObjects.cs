using System.Collections.Generic;
using UnityEngine;

public class AutoEnableGameObjects : MonoBehaviour
{
    public List<GameObject> Objects;
    
    // Start is called before the first frame update
    void Start()
    {
        foreach (var obj in Objects)
        {
            obj.SetActive(true);
        }
    }
}
